FROM java:8
COPY build/libs/p2-l4g-0.0.1-SNAPSHOT.jar .
EXPOSE 80
CMD java -jar p2-l4g-0.0.1-SNAPSHOT.jar
