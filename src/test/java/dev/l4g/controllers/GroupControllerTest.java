package dev.l4g.controllers;

import dev.l4g.models.Group;
import dev.l4g.services.GroupService;
import dev.l4g.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ExtendWith(MockitoExtension.class)
public class GroupControllerTest {

        private MockMvc mockMvc;

//        @Autowired
//        private TestRestTemplate restTemplate;

        @MockBean
        GroupService groupService;

        @InjectMocks
        GroupController groupController;

        @BeforeEach
        public void setup() {
            this.mockMvc = MockMvcBuilders.standaloneSetup(groupController).build();
        }

        @Test
        public void testGetAllShouldReturnAllGroups() throws Exception {
            List<Group> groups = new ArrayList<>();
            groups.add(new Group(1, "Smashbros", "desc1"));
            groups.add(new Group(2, "Mario Kart", "desc2"));
            groups.add(new Group(3, "Destiny 2", "desc3"));

            when(groupService.getAll()).thenReturn(groups);

            this.mockMvc.perform(get("/groups").header("Authorization", "user-token"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$[*].name")
                        .value(hasItems("Smashbros", "Mario Kart", "Destiny 2")));
        }

        @Test
        public void testGetByIdShouldReturnCorrectGroup() throws Exception {
            Group group = new Group(1, "Smashbros", "desc1");

            when(groupService.getGroupById(1)).thenReturn(group);

                this.mockMvc.perform(get("/groups/{id}", 1).header("Authorization", "user-token"))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                        .andExpect(jsonPath("$.id", is(1)))
                        .andExpect(jsonPath("$.name", is("Smashbros")));
        }


        /*
        ///////////
        FAILS TEST
        ///////////
         */
        @Test
        public void testCreateGroupShouldReturnOk() throws Exception {
            this.mockMvc.perform(post("/groups")
                    .param("name", "A new Group")
                    .param("userId", "100")
                    .header("Authorization", "user-token")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                    .andExpect(status().isOk());
        }

        /*
        //////////
        FAILS TEST
        //////////
         */
        @Test
        public void testGetByBadIdShouldReturnNotFound() throws Exception{
            when(groupService.getGroupById(20)).thenThrow(EntityNotFoundException.class);

            this.mockMvc.perform(get("/groups/{id}", 20).header("Authorization", "user-token"))
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath("$.error", is("No Group found with id: 20")));
        }


    }
