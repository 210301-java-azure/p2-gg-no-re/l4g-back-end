package dev.l4g.controllers;

import dev.l4g.dto.UserGroupDTO;
import dev.l4g.models.Group;
import dev.l4g.models.User;
import dev.l4g.models.UserGroup;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestReporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private User user;

    @Test
    public void testGetByIdShouldReturnCorrectUser() {
        User actual = this.restTemplate.getForObject("http://localhost:8082/users/1", User.class);
        User expected = new User(1, "Duncan", "duncan@gmail.com", "12345", 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetByBadIdShouldReturnNotFound() {
        User actual = this.restTemplate.getForObject("http://localhost:8082/users/100", User.class, HttpStatus.NOT_FOUND);
        User expected = new User(0, null, null, null, 0);
        assertEquals(actual, expected);

    }

    // Fails because of JSON Deserialize error
//    @Test
//    public void testGetAllUsersInGroupShouldReturnAllUsers() {
//        List<UserGroupDTO> actual = this.restTemplate.getForObject("http://localhost:8082/groups/1", List.class);
//        List<UserGroupDTO> expected = new ArrayList<>();
//        List<Group> groups = new ArrayList<>();
//        Group group = new Group(1, "Smashbros");
//        groups.add(group);
//        UserGroupDTO user1 = new UserGroupDTO(1, "Duncan", groups);
//        UserGroupDTO user2 = new UserGroupDTO(2, "Marvin", groups);
//        UserGroupDTO user3 = new UserGroupDTO(3, "Betsy", groups);
//        expected.add(user1);
//        expected.add(user2);
//        expected.add(user3);
//        assertEquals(actual, expected);
//    }


}
