DROP TABLE IF EXISTS users;
CREATE TABLE users (
	id			int IDENTITY PRIMARY KEY,
	username	varchar(255),
	password	varchar(255),
	user_role	int,
	email	    varchar(255)
);

DROP TABLE IF EXISTS groups;
CREATE TABLE groups (
	id			int IDENTITY PRIMARY KEY,
	name		varchar(255)
);


DROP TABLE IF EXISTS discussion;
CREATE TABLE discussion (
	id			int IDENTITY PRIMARY KEY,
	user_id		int,
	group_id	int,
	username	varchar(255),
	title		varchar(255)
);


DROP TABLE IF EXISTS comment;
CREATE TABLE comment (
	id				int IDENTITY PRIMARY KEY,
	discussion_id	int,
	user_id			int,
	parent_id		int,
	message			varchar,
	date_posted     timestamp
);

DROP TABLE IF EXISTS user_group;
CREATE TABLE user_group (
	id			int IDENTITY PRIMARY KEY,
	group_id	int,
	user_id		int,
	user_status int
);

INSERT INTO users
VALUES (1, 'Duncan', '12345', 1, 'duncan@gmail.com' );

INSERT INTO users
VALUES (2, 'Marvin', 'secret', 2, 'marvin@gmail.com');

INSERT INTO users
VALUES (3, 'Betsy', 'pope', 2, 'betsy@gmail.com');

INSERT INTO groups
VALUES (1, 'Smashbros');

INSERT INTO groups
VALUES (2, 'Mario Kart');

INSERT INTO user_group
VALUES (1, 1, 1, 1);
INSERT INTO user_group
VALUES (2, 1, 2, 2);
INSERT INTO user_group
VALUES (3, 1, 3, 2);