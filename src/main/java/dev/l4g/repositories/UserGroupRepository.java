package dev.l4g.repositories;

import dev.l4g.models.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup,Integer>{

    @Query(value = "SELECT * FROM user_group WHERE user_id = ?1 AND group_id = ?2", nativeQuery = true)
    public UserGroup findUniqueId(int userId, int group_id);

    public int countByGroupId(int groupId);

    public List<UserGroup> findByGroupId(int groupId);
}
