package dev.l4g.repositories;

import dev.l4g.models.Group;
import dev.l4g.models.Image;
import dev.l4g.models.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {

    @Query(value = "SELECT * FROM images WHERE  group_id = ?1", nativeQuery = true)
    public Image findByGroupId(int id);

    @Query(value = "DELETE FROM images WHERE group_id = ?1", nativeQuery = true)
    public void deleteByGroupId(int groupId);
}

