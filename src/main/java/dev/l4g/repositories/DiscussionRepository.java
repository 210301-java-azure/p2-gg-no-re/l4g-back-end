package dev.l4g.repositories;

import dev.l4g.models.Discussion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiscussionRepository extends JpaRepository<Discussion, Integer> {
    public List<Discussion> findDiscussionByGroupId(int groupId);

    @Query(value = "SELECT * FROM discussion WHERE user_id = ?1", nativeQuery = true)
    public List<Discussion> findDiscussionByUserId(int user_id);
}
