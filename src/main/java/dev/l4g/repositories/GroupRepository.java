package dev.l4g.repositories;

import dev.l4g.models.Group;
import dev.l4g.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer>{
    public Group findGroupById(int id);
    public List<Group> findGroupByNameContaining(String name);
}

