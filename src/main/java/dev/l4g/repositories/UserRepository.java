package dev.l4g.repositories;

import dev.l4g.dto.UserDTO;
import dev.l4g.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    //@Query("SELECT s from Users u WHERE u.email =?1")
    Optional<User> findUserByEmail(String email);
    Optional<User> findUserByUsername(String username);

    //public List<Item> getUsersByName(String name);
    public List<User> findUsersByUsernameContaining(String someVariable);

    public List<User> findUsersByEmailContaining(String someVariable);


    public User findUsersByUsername(String someVariable);

    public User findUsersByEmail(String someVariable);
}
