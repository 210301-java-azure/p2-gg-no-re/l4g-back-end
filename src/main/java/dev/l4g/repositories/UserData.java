package dev.l4g.repositories;

import dev.l4g.models.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Repository
public class UserData  {

    private final List<User> users = new ArrayList<>();

    public UserData(){
        super();
        users.add(new User(1, "johndove", "johndove@gmail.com","123456", 1));
        users.add(new User(2, "davidsmith", "davidsmith@gmail.com","111111", 2));
        users.add(new User(3, "crehm", "crehm@gmail.com","supersecret", 3));

    }

    //@Override
    public List<User> findAll() {
        return new ArrayList<>(users);
    }

   // @Override
    public User getUserById(int id) {
        return users.stream().filter(user -> user.getId()==id).findAny().orElse(null);
    }

    //@Override
    public User addUser(User user) {
        users.add(user);
        return user;
    }

    //@Override
    public List<User> getUsersByName(String username) {
        return users.stream()
                .filter(user -> user.getUsername().toLowerCase().contains(username.toLowerCase()))
                .collect(Collectors.toList());
    }
}
