package dev.l4g.repositories;

import dev.l4g.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{
    public List<Comment> findDiscussionByDiscussionId(int discussionId);
    public List<Comment> findDiscussionByUserId(int userId);
    public List<Comment> findRepliesByParentId(int parentId);
    public Long deleteByDiscussionId(int id);
    public Long deleteByParentId(int id);
}
