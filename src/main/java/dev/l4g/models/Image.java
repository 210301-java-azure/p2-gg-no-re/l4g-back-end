package dev.l4g.models;

import org.hibernate.annotations.Type;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Arrays;

@Entity
@Table(name = "images")
@Component
@Scope("prototype")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "group_id")
    private int groupId;
    @Column(name = "user_id")
    private int userId;
    @Column(name = "picture")
    @Type(type="org.hibernate.type.ImageType")
    private byte[] image;

    public Image() {

    }

    public Image(int id, Integer groupId, int userId, byte[] image) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", userId=" + userId +
                ", image=" + Arrays.toString(image) +
                '}';
    }
}
