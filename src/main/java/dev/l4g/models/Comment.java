package dev.l4g.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name="comment")
@Component
@Scope("prototype")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "discussion_id")
    private int discussionId;
    @Column(name = "user_id")
    private int userId;
    @JsonInclude(JsonInclude.Include.ALWAYS)
    @Column(name = "parent_id")
    private Integer parentId;
    @Column(name= "username")
    private String username;
    @Column(name = "message", columnDefinition = "TEXT")
    private String message;
//    @Column(name = "username")
//    private String username;
    @CreationTimestamp
    @Column(name = "date_posted", columnDefinition = "TIMESTAMP")
    Timestamp datePosted;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "discussion_id", insertable = false, updatable = false)
    public Discussion discussion;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    public User user;

    public Comment() {
        //super();
    }

    public Comment(int id, int discussionId, int userId, Integer parentId, String message, Timestamp datePosted, String username) {
        this.id = id;
        this.discussionId = discussionId;
        this.userId = userId;
        this.parentId = parentId;
        this.message = message;
//        this.username = username;
        this.datePosted = datePosted;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscussionId() {
        return discussionId;
    }

    public void setDiscussionId(int discussionId) {
        this.discussionId = discussionId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Timestamp datePosted) {
        this.datePosted = datePosted;
    }

//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id && discussionId == comment.discussionId && userId == comment.userId && parentId == comment.parentId && Objects.equals(message, comment.message) && Objects.equals(datePosted, comment.datePosted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, discussionId, userId, parentId, message, datePosted);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", discussionId=" + discussionId +
                ", userId=" + userId +
                ", parentId=" + parentId +
                ", message='" + message + '\'' +
                ", datePosted='" + datePosted + '\'' +
                '}';
    }
}
