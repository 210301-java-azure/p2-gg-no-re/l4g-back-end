package dev.l4g.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name="users")
@Component
@Scope("prototype")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class User {

    @Id
    @JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@id", scope = User.class)
    @SequenceGenerator( name="id", sequenceName = "id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY) //.IDENTITY)
    private int id;
    private String username;
    private String email;
    private String password;
    @Column(name = "user_role")
    private int userRole;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
    name = "user_group",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    public List<Group> groups;

    public User() {
        //super();
    }

    public User(int id, String username, String email, String password, int userRole, List<Group> groups) {
        this.id = id;
        this.username = username;
        this.email    = email;
        this.password = password;
        this.userRole = userRole;
        this.groups   = groups;
    }

    public User(String username, String email, String password, int userRole) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.userRole = userRole;
    }

    public User(int id, String username, String email, String password, int userRole) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.userRole = userRole;
    }

    public User(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && userRole == user.userRole
                && Objects.equals(username, user.username)
                && Objects.equals(email, user.email)
                && Objects.equals(password, user.password);
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }



    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, password, userRole);
    }

    @Override
    public String toString() {
            return "User{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    ", userRole=" + userRole +
                    '}';

    }
}
