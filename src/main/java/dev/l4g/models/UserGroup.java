package dev.l4g.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name ="user_group")
public class UserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "group_id")
    private int groupId;
    @Column(name = "user_id")
    private int userId;
    @Column(name = "user_status")
    private int status;

    public UserGroup() {
        //super();
    }

    public UserGroup(int id, int groupId, int userId, int status) {
        this.id = id;
        this.groupId = groupId;
        this.userId = userId;
        this.status = status;
    }

    public UserGroup(int groupId, int userId, int status) {
        this.groupId = groupId;
        this.userId = userId;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserGroup userGroup = (UserGroup) o;
        return id == userGroup.id && groupId == userGroup.groupId && userId == userGroup.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, groupId, userId);
    }

    @Override
    public String toString() {
        return "UserGroup{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", userId=" + userId +
                '}';
    }
}
