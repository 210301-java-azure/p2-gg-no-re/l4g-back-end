package dev.l4g.controllers;

import dev.l4g.dto.UserDTO;
import dev.l4g.models.User;
import dev.l4g.dto.UserGroupDTO;

import dev.l4g.services.UserService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
public class UserController {

    private static Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll(@RequestParam(value = "username",required = false)String usernameParam,
                                                @RequestParam(value = "email",required = false)String emailParam) {
        if(usernameParam != null){
            //logger.info("Getting users by username: {}", usernameParam);
            return ResponseEntity.ok().body(userService.getByUsername(usernameParam));
        }

        if(emailParam != null){
          //logger.info("Getting users by email: {}", emailParam);
            return ResponseEntity.ok().body(userService.getByEmail(emailParam));
        }
        //logger.info("Getting all users");
        return ResponseEntity.ok().body(userService.getAll());
    }

    //get user by Id
    @GetMapping(value = "/{id}",produces = "application/json")
    public ResponseEntity<User> getUserById(@PathVariable("id") int id){
        //logger.info("Getting user with id: {}", id);
        return ResponseEntity.ok().body(userService.getById(id));
    }

    // Admin user only can delete any user record/profile
    @DeleteMapping(value = "/{id}")
    public void deleteUserById(@PathVariable("id") int id, @RequestHeader("Authorization") String token){

        logger.info("Login as Admin User {} ", token);
        if( !token.equals("admin-token") ){
            logger.info("As Admin user, only able to delete other user profile"+ token);
            new ResponseEntity("user-token does not have the permission to delete other user profile", HttpStatus.UNAUTHORIZED);
        }

        logger.info("Delete user with id: {}", id);
        userService.delete(id);
        ResponseEntity.ok();
    }

    // Admin user only can change any user's Role Id
    @PutMapping(value = "/role/{id}")
    public ResponseEntity<Integer> updateUserRole(@PathVariable("id") int id, @RequestParam(value="roleId") int roleId,
                                                  @RequestHeader("Authorization") String token){

        if( !token.equals("admin-token") ){
            logger.info("As Admin user, only able to change other user's role ID");
            return new ResponseEntity("user-token does not have the permission to change the Role", HttpStatus.UNAUTHORIZED);
        }
        int newRole = userService.updateRole(id, roleId);
        if( newRole == 0){
            logger.info("No changes made to user Role");
            return new ResponseEntity("No changes made to user Role", HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(newRole, HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public ResponseEntity<User> updateUser(@PathVariable("id") int id, @RequestBody User updatedUser ){
            userService.update(id, updatedUser);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("groups/{id}")
    public ResponseEntity<List<UserGroupDTO>> getAllUsersInGroup(@PathVariable(value = "id") int id) {
        List<UserGroupDTO> groups = new ArrayList<>();
        groups = userService.getAllUsersInGroupId(id);
        return ResponseEntity.ok().body(groups);
    }

    @GetMapping(value = "{id}/groups")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<UserGroupDTO>> getAllGroupsForUser(@PathVariable("id") int id) {
        List<UserGroupDTO> groups = new ArrayList<>();
        groups = userService.getAllGroupsForUserId(id);
        return ResponseEntity.ok().body(groups);
    }
}

