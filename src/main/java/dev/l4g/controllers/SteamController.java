package dev.l4g.controllers;

import dev.l4g.services.SteamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/steam")
public class SteamController {

    private final SteamService steamService;

    @Autowired
    public SteamController(SteamService steamService){
        this.steamService = steamService;
    }


    @GetMapping
    public ResponseEntity<String> getNewsByAppId(@RequestParam(value = "appid") Integer steamAppId){
        return steamService.getNewsByAppId(steamAppId);
    }
}
