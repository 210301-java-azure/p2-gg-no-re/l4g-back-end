package dev.l4g.controllers;

import dev.l4g.models.Group;
import dev.l4g.models.User;
import dev.l4g.models.UserGroup;
import dev.l4g.repositories.UserGroupRepository;
import dev.l4g.repositories.UserRepository;
import dev.l4g.services.GroupService;
import dev.l4g.services.UserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/groups")
@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserGroupService userGroupService;

    @Autowired
    private UserGroupRepository userGroupRepo;

    @Autowired
    private UserRepository userRepo;


    // Grab all Groups in Database
    @GetMapping
    public ResponseEntity<List<Group>> getAllGroups(@RequestParam(name="name", required = false) String name) {
        if(name != null)
            return ResponseEntity.ok().body(groupService.getGroupsByName(name));
        return ResponseEntity.ok().body(groupService.getAll());
    }

    // Grab a specific group by its Id
    @GetMapping("/{id}")
    public ResponseEntity<Group> getGroupById(@PathVariable(name = "id") int id) {
        return ResponseEntity.ok().body(groupService.getGroupById(id));
    }

    /*
    ////////////////////////////
    THIS COULD POTENTIALLY CAUSE AN ERROR ON THE FRONT END
    CAREFUL ABOUT THE "application/x-www.form-urlencoded"!
    ////////////////////////////

     */
    // Creates a new group and automatically makes the User a mod
    @PostMapping
    public ResponseEntity<Group> createNewGroup(@RequestParam(value = "name") String name,
                                @RequestParam(value = "userId") int userId,
                                @RequestParam(value = "description") String description) {
        // Create the group first, we'll need its ID later
        Group group = new Group();
        group.setName(name);
        group.setDescription(description);
        group = groupService.createGroup(group);

        // Create a UserGroup to link new group to the user that made it
        // automatically assign moderator privileges to user
        UserGroup userGroup = new UserGroup();
        userGroup.setUserId(userId);
        userGroup.setGroupId(group.getId());
        userGroup.setStatus(1);
        userGroupService.createUserGroup((userGroup));
        return new ResponseEntity(group, HttpStatus.OK);
    }

    // Updates a group, must be in JSON format
    @PutMapping(consumes = "application/json")
        public void editGroup(@RequestBody Group alteredGroup) {
        groupService.editGroup(alteredGroup);
    }

    // Delete a group by its ID
    @DeleteMapping("/{id}")
    public void deleteGroup(@PathVariable(value = "id") int id) {
        groupService.deleteGroup(id);
    }
}
