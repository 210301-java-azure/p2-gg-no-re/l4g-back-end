package dev.l4g.controllers;

import dev.l4g.models.Image;
import dev.l4g.models.UserGroup;
import dev.l4g.repositories.ImageRepository;
import dev.l4g.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
@RequestMapping("/image")
public class ImageController {

    @Autowired
    ImageService imageService;

    @Autowired
    ImageRepository imageRepo;

    @GetMapping
    public ResponseEntity<List<Image>> getAllImages() {
        return ResponseEntity.ok().body(imageService.getAllImages());
    }

    @GetMapping(value = "/group/{id}", produces="application/json")
    public ResponseEntity<Image> getImageForGroup(@PathVariable("id") Integer id) {
        System.out.println("id is: " + id);
        return ResponseEntity.ok().body(imageService.getImageByGroup(id));

    }

    @PostMapping("/upload")
    public ResponseEntity<Image> uploadImage(
            @RequestParam("groupId") int groupId,
            @RequestParam("userId") int userId,
            @RequestParam("imageFile")MultipartFile file) throws IOException {
        System.out.println("original file size bytes: " + file.getBytes().length);
        Image img = new Image();
        img.setGroupId(groupId);
        img.setUserId(userId);
        img.setImage(file.getBytes());
        imageRepo.save(img);
        return new ResponseEntity(img, HttpStatus.OK);
    }

    @DeleteMapping("/{groupId}")
    public ResponseEntity<Image> deleteImage (@PathVariable("groupId") int groupId){
        imageService.deleteImage(groupId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
