package dev.l4g.controllers;

import dev.l4g.models.UserGroup;
import dev.l4g.services.UserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user-group")
@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
public class UserGroupController {

    @Autowired
    UserGroupService userGroupService;

    @GetMapping
    public ResponseEntity<List<UserGroup>> getAllUserGroups() {
        return ResponseEntity.ok().body(userGroupService.getAll());
    }


    @GetMapping("/user/{userId}/group/{groupId}")
    public ResponseEntity<UserGroup> checkForUserInGroup(@PathVariable(value = "userId") int userId,
                                                        @PathVariable(value = "groupId") int groupId){
        return ResponseEntity.ok().body(userGroupService.userInGroup(userId, groupId));
    }

    // Add a user to a group
    // must be JSON format
    @PostMapping(consumes = "application/json")
    public ResponseEntity<UserGroup> addUserToGroup(@RequestBody UserGroup addUser) {
        UserGroup ug = userGroupService.addUserToGroup(addUser);
        if(ug == null)
            return ResponseEntity.status(400).body(null);
        return ResponseEntity.ok().body(ug);
    }

    // Edit a user's moderator status of a group
    @PutMapping(consumes = "application/json")
    public void editModerator(@RequestBody UserGroup editMod) {
        System.out.println("in Controller: editMod status: " + editMod.getStatus());
        userGroupService.editModerator(editMod);
    }

    // Delete a user from a group using user id
    @DeleteMapping("/user/{userId}/group/{groupId}")
    public void deleteUserFromGroup(@PathVariable(value = "userId") int userId,
                                    @PathVariable(value = "groupId") int groupId){
        userGroupService.deleteUserFromGroup(userId, groupId);
    }


}
