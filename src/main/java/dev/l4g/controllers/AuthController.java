package dev.l4g.controllers;

import dev.l4g.models.User;
import dev.l4g.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import dev.l4g.util.PasswordSecurity;

@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
@RestController
public class AuthController extends PasswordSecurity{

    private static Logger logger = LogManager.getLogger(AuthController.class);

    @Autowired
    private UserService userService;
    private User user;


    @PostMapping("/login")
    public ResponseEntity login(@RequestParam("email")String email,
                                @RequestParam("password")String password) {

        //getAll users from db verify if user exist in the database
        User user = userService.getUserByEmail(email);

        // Check if users exists
        if(user != null && email.equals(user.getEmail() )  ) {

            // Check the input password against user's stored hashPassword
            Boolean verified = checkPassword(password, user.getPassword());

            // Check if passwords match
            if(verified) {

                // Check if user is an Admin
                if(user.getUserRole() == 1) {
                    return ResponseEntity.ok()
                            .header("Authorization", "admin-token")
                            .header("User-Id", String.valueOf(user.getId()))
                            .header("Username", user.getUsername())
                            .header("Access-Control-Expose-Headers", "Authorization", "User-ID", "Username")
                            .build();
                }

                // Check if user is a regular user
                if(user.getUserRole() == 2) {
                    return ResponseEntity.ok()
                            .header("Authorization", "user-token")
                            .header("User-Id", String.valueOf(user.getId()))
                            .header("Username", user.getUsername())
                            .header("Access-Control-Expose-Headers", "Authorization", "User-ID", "Username")
                            .build();
                }

                // If for some reason, user has a non-existent role, return an error
                if(user.getUserRole() > 2 ) {
                    return new ResponseEntity("User cannot have such role", HttpStatus.UNAUTHORIZED);
                }
            } else{

                // Throw error if password is incorrect
                return new ResponseEntity<>("password not recognized", HttpStatus.UNAUTHORIZED);
            }
        } else{
            // Throw error if user is non-existent
            return new ResponseEntity<>("email not recognized", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    // User Signup Method
    @CrossOrigin(origins = "http://l4g.blob.core.windows.net")
    @PostMapping("/signup")
    public ResponseEntity signup(@RequestParam("username")String username,
                                       @RequestParam("email")String email,
                                       @RequestParam("password")String password,
                                       @RequestParam("userRole")Integer userRole) {

        if(username == null || username.length() == 0) {
            logger.info("Username ==>"+username+" can not be null");
            return new ResponseEntity("username can not be empty", HttpStatus.UNAUTHORIZED);
        }

        if(email == null || email.length() == 0 ) {
            logger.info("email ==>"+email+" can not be null");
            return new ResponseEntity("email can not be empty", HttpStatus.UNAUTHORIZED);
        }

        if(password == null || password.length() == 0 ) {
            logger.info("password ==>"+password+" can not be null");
            return new ResponseEntity("password can not be empty", HttpStatus.UNAUTHORIZED);
        }

        System.out.println("username:" + username + ",  email: " + email +
                           ",  password: " + password+",  userRole: " + userRole);
        User user = new User(username, email, password, userRole);
        System.out.println("current user before create method call: " + user.toString());

        // Hash the users password
        String hashedPass = hashPass(password);

        //Create User with hashed password
        user = new User(username, email, hashedPass, userRole);
        userService.create(user);

        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}