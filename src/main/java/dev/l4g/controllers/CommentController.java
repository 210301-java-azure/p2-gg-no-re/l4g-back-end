package dev.l4g.controllers;

import dev.l4g.models.Comment;
import dev.l4g.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comments")
@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping
    public ResponseEntity<List<Comment>> getAllComments() {
        return ResponseEntity.ok().body(commentService.getAll());
    }

    //Get all comments from a certain discussion
    @GetMapping("/discussion/{id}")
    public ResponseEntity<List<Comment>> getAllDiscussionComments(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok().body(commentService.getCommentsByDiscussionId(id));
    }

    // Get all Replies to a comment
    @GetMapping("{id}/replies")
    public ResponseEntity<List<Comment>> getAllReplies(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok().body(commentService.getRepliesByParentId(id));
    }

    // Get all comments from a certain user
    @GetMapping("/user/{id}")
    public ResponseEntity<List<Comment>> getAllUserComments(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok().body(commentService.getCommentsByUserId(id));
    }

    // Make a new comment
    @PostMapping(consumes = "application/json")
    public ResponseEntity<Comment> createNewComment(@RequestBody Comment comment) {
        commentService.createComment(comment);
        return new ResponseEntity<>(comment, HttpStatus.CREATED);
    }

    // Edit a comment
    @PutMapping(consumes = "application/json")
    public ResponseEntity<Comment> editComment(@RequestBody Comment alteredComment) {
        commentService.editComment(alteredComment);
        return new ResponseEntity<>(alteredComment, HttpStatus.OK);
    }

    // Maybe just set the message value to "deleted"? otherwise
    // comment replies won't have a parentId to reference
    // Delete a comment by its ID
    @DeleteMapping("/{id}")
    public ResponseEntity deleteComment(@PathVariable(value = "id") int id) {
        commentService.deleteComment(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
