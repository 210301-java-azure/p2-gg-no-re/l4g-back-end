package dev.l4g.controllers;

import dev.l4g.models.Discussion;
import dev.l4g.models.Group;
import dev.l4g.repositories.DiscussionRepository;
import dev.l4g.services.DiscussionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://l4g.blob.core.windows.net")
@RequestMapping("/discussions")
public class DiscussionController {

    @Autowired
    private DiscussionRepository discussionRepo;

    @Autowired
    private DiscussionService discussionService;

    // Get all Discussions In a group
    @GetMapping("/{groupId}/group")
    public ResponseEntity<List<Discussion>> getAllDiscussionForId(@PathVariable(value = "groupId") int groupId) {
        System.out.println("group discussion controller called");
        return ResponseEntity.ok().body(discussionService.getDiscussionsByGroupId(groupId));
    }

    // Get all discussions that a specific user started
    @GetMapping("/{userId}/user")
    public ResponseEntity<List<Discussion>> getAllDiscussionForUserId(@PathVariable(value = "userId") int userId) {
        return ResponseEntity.ok().body(discussionService.getDiscussionByUserId(userId));
    }

    // Create a new discussion
    // Must be in JSON format
    @PostMapping(consumes = "application/json")
    public ResponseEntity<Discussion> createNewDiscussion(@RequestBody Discussion discussion) {

        return ResponseEntity.ok().body(discussionService.createDiscussion(discussion));
    }

    // Edit a Discussion by Discussion JSON object
    @PutMapping(consumes = "application/json")
    public void editGroup(@RequestBody Discussion alteredDiscussion) {
        discussionService.editDiscussion(alteredDiscussion);
    }


    // Delete a discussion by its ID
    @DeleteMapping("/{id}")
    public @ResponseBody void deleteDiscussion(@PathVariable(value = "id") int id) {
        discussionService.deleteDiscussion(id);
    }

}
