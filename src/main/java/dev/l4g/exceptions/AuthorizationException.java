package dev.l4g.exceptions;

public class AuthorizationException extends RuntimeException{

    // Default Message
    public AuthorizationException() {
        super("User is unauthorized for this request");
    }

    // Custom Message
    public AuthorizationException(String message) {
        super(message);
    }
}
