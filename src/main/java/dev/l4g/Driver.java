package dev.l4g;

import io.javalin.Javalin;
import io.javalin.websocket.WsContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@SpringBootApplication
public class Driver {
    static class Message {
        private String user;
        private String text;

        public Message() {
        }

        public Message(String user, String text) {
            this.user = user;
            this.text = text;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
    private static Logger logger = LoggerFactory.getLogger(Driver.class);
    private static ConcurrentHashMap<String, List<WsContext>> conMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        SpringApplication.run(Driver.class, args);

        Javalin app = Javalin.create().start(7000);
        app.ws("/messages/:group", ws -> {
            ws.onConnect(ctx -> {
                String group = ctx.pathParam("group");
                System.out.println("Connecting to: " + group);
                List<WsContext> list = conMap.get(group);
                if(list != null) {
                    list.add(ctx);
                } else {
                    System.out.println("adding group: " + group);
                    list = Collections.synchronizedList(new LinkedList<WsContext>());
                    list.add(ctx);
                    conMap.put(group, list);
                }
                int members = list.size();
                Message m = new Message("[System]", "Connected to "+group+" ("+members+
                        (members == 1 ? " member)" : " members)"));
                ctx.send(m);

                list.forEach(c -> {
                    if(c.equals(ctx)) return;

                    c.send(new Message("[System]", "A user has connected ("+members+
                        (members == 1 ? " member)" : " members)")));
                });
            });

            ws.onMessage(ctx -> {
                Message msg = ctx.message(Message.class);
                String group = ctx.pathParam("group");
                List<WsContext> list = conMap.get(group);

                list.forEach(c -> {
                    if(!c.equals(ctx))
                        c.send(msg);
                });
            });

            ws.onClose(ctx -> {
                String group = ctx.pathParam("group");
                System.out.println("Closing connection to " + group);
                List<WsContext> list = conMap.get(group);

                list.remove(ctx);
                int members = list.size();
                if(members == 0) {
                    System.out.println("removing group: " + group);
                    conMap.remove(group);
                    return;
                }
                list.forEach(c -> {
                    Message m = new Message("[System]", "A user has disconnected (" + members +
                            (members == 1 ? " member)" : " members)"));
                    c.send(m);
                });
            });
        });
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/groups");
                registry.addMapping("/steam").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200");
                registry.addMapping("/groups").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200");
                registry.addMapping("/discussions").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200")
                    .allowedMethods("POST", "PUT", "DELETE");
                registry.addMapping("/users/{id}/groups");
                registry.addMapping("/comments/discussion/{id}");
                registry.addMapping("/comments");
                registry.addMapping("/users");
                registry.addMapping("/users/{id}");
                registry.addMapping("/users/role/{id}");
                registry.addMapping("/user-group");
                registry.addMapping("/discussions").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200");
                registry.addMapping("/user-groups").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200");
                registry.addMapping("/comments/discussion/{id}").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200");
                registry.addMapping("/users").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200");
                registry.addMapping("/users/{id}").allowedOrigins("http://l4g.blob.core.windows.net", "http://192.168.0.2:4200").allowedMethods("*");
                registry.addMapping("/**");
            }
        };
    }
}