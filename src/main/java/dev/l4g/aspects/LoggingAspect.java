package dev.l4g.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.Arrays;

@Aspect
@Component
public class LoggingAspect {

    private Logger logger = LogManager.getLogger(LoggingAspect.class);

    @Pointcut("within(dev.l4g..*)")
    public void logAllPointCut() {};

    @Before("logAllPointCut()")
    public void logMethodStart(JoinPoint jp) {
        String methodSig = extractMethodSignature(jp);
        String argStr = Arrays.toString(jp.getArgs());
        logger.info("{} invoked at {}; input arguments: {}", methodSig, LocalDateTime.now(), argStr);
    }

    private String extractMethodSignature(JoinPoint jp) {
        return jp.getTarget().getClass().toString() + "." + jp.getSignature().getName();
    }

    @AfterReturning(pointcut = "logAllPointCut()", returning = "returned")
    public void logMethodReturned(JoinPoint jp, Object returned) {
        String methodSig = extractMethodSignature(jp);
        logger.info("{} was successfully returned at {} with a value of {}", methodSig, LocalDateTime.now(), returned);

    }

}
