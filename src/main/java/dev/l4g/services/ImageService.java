package dev.l4g.services;

import dev.l4g.models.Image;
import dev.l4g.repositories.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageService {

    @Autowired
    ImageRepository imageRepo;

    public List<Image> getAllImages() {
        return imageRepo.findAll();
    }

    public Image getImageByGroup(int id) {
        return imageRepo.findByGroupId(id);
    }

    public void deleteImage(int groupId) {
        System.out.println("groupId in service: " + groupId);
        imageRepo.deleteByGroupId(groupId);
    }

}
