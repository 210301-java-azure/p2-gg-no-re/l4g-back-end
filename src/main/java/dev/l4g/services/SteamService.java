package dev.l4g.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class SteamService {


    @Autowired
    private RestTemplate restTemplate;
    ObjectMapper objectMapper = new ObjectMapper();
    private final String WEB_NEWS_URL = "http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/";

    public Object getNewsByAppId(int steamAppId, int count){

        return null;
    }

    public ResponseEntity<String> getNewsByAppId(Integer steamAppId) {
        return restTemplate.getForEntity(WEB_NEWS_URL + "?appid=" + steamAppId + "&count=1", String.class);
    }
}
