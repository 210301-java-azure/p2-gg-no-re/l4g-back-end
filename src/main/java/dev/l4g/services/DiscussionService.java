package dev.l4g.services;

import dev.l4g.models.Discussion;
import dev.l4g.models.Group;
import dev.l4g.repositories.CommentRepository;
import dev.l4g.repositories.DiscussionRepository;
import dev.l4g.repositories.GroupRepository;
import dev.l4g.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class DiscussionService {

    @Autowired
    private DiscussionRepository discussionRepo;

    @Autowired
    private GroupRepository groupRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private CommentRepository commentRepo;

    public List<Discussion> getDiscussionsByGroupId(int groupId) {
        List<Discussion> discussions = discussionRepo.findDiscussionByGroupId(groupId);
        System.out.println("group discussion servcie called");
        if (discussions.isEmpty()) {
            throw new EntityNotFoundException("No discussions for group with id: " + groupId);
        }
        return discussions;
    }

    public List<Discussion> getDiscussionByUserId(int userId) {
        System.out.println("user disuccsion servic ecalled");
        return discussionRepo.findDiscussionByUserId(userId);
    }

    public Discussion createDiscussion(Discussion discussion){
        return discussionRepo.save(discussion);
    }

    public void editDiscussion(Discussion alteredDiscussion) {
        Optional<Discussion> option = discussionRepo.findById(alteredDiscussion.getId());
        Discussion discussionToEdit = option.get();
        discussionToEdit.setTitle(alteredDiscussion.getTitle());
        discussionRepo.save(discussionToEdit);
    }

    @Transactional
    public void deleteDiscussion(int id) {
        Optional<Discussion> option = discussionRepo.findById(id);
        Discussion discussion = option.get();
        commentRepo.deleteByDiscussionId(discussion.getId());
        discussionRepo.delete(discussion);
    }

}
