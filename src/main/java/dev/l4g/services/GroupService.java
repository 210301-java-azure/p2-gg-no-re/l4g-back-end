package dev.l4g.services;

import dev.l4g.models.Discussion;
import dev.l4g.models.Group;
import dev.l4g.models.User;
import dev.l4g.models.UserGroup;
import dev.l4g.repositories.GroupRepository;
import dev.l4g.repositories.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    public List<Group> getAll() {
        List<Group> allGroups = groupRepository.findAll();
        if (allGroups.isEmpty()) {
            throw new EntityNotFoundException();
        }
        allGroups.forEach(g -> {
            g.setNumMembers(userGroupRepository.countByGroupId(g.getId()));
        });
        return allGroups;
    }

    public Group getGroupById(int id) {
//        Optional<Group> option = groupRepository.findById(id);
//        Group group = option.get();
        Group group = groupRepository.findGroupById(id);
        if (group == null) {
            throw new EntityNotFoundException("No group found with id: " + id);
        }
        group.setNumMembers(userGroupRepository.countByGroupId(group.getId()));
        return group;
    }

    public List<Group> getGroupsByName(String name) {
        List<Group> groups = groupRepository.findGroupByNameContaining(name);
        if (groups.isEmpty()) {
            throw new EntityNotFoundException();
        }
        groups.forEach(g -> {
            g.setNumMembers(userGroupRepository.countByGroupId(g.getId()));
        });
        return groups;
    }


    public Group createGroup(Group group) {
        return groupRepository.save(group);
    }

    public void editGroup(Group alteredGroup) {
//        Optional<Group> option = groupRepository.findById(alteredGroup.getId());
//        Group groupToEdit = option.get();
        System.out.println(alteredGroup);
        Group groupToEdit = groupRepository.findGroupById(alteredGroup.getId());
        if (groupToEdit == null) {
            throw new EntityNotFoundException("No group found with id: " + alteredGroup.getId());
        }
        groupToEdit.setName(alteredGroup.getName());
        groupToEdit.setDescription(alteredGroup.getDescription());
        groupRepository.save(groupToEdit);
    }

    public void deleteGroup(int id) {
//        Optional<Group> option = groupRepository.findById(id);
//        Group group = option.get();
        Group group = groupRepository.findGroupById(id);
        if (group == null) {
            throw new EntityNotFoundException("No Group found with id: " + id);
        }
        groupRepository.delete(group);
    }
}

