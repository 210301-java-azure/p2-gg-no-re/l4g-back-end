package dev.l4g.services;

import ch.qos.logback.core.net.SyslogOutputStream;
import dev.l4g.controllers.AuthController;
import dev.l4g.dto.UserDTO;
import dev.l4g.dto.UserGroupDTO;

import dev.l4g.models.Group;
import dev.l4g.models.User;
import dev.l4g.repositories.UserGroupRepository;
import dev.l4g.repositories.UserRepository;
import dev.l4g.util.PasswordSecurity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private static Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    UserRepository userRepo;

    @Autowired
    UserGroupRepository userGroupRepo;

    @Autowired
    PasswordSecurity passwordSecurity;

    // This method grabs users
    public List<UserDTO> getAll() {
        List<User> users = userRepo.findAll();
        List<UserDTO> usersDto = new ArrayList<>();
        for (User user : users) {
            UserDTO userDto = new UserDTO();
            userDto.setId(user.getId());
            userDto.setUsername(user.getUsername());
            userDto.setEmail(user.getEmail());
            userDto.setUserRole(user.getUserRole());
            usersDto.add(userDto);
        }
            return usersDto;
        }


    public User getById(int id){
        return userRepo.findById(id).get();
    }

    public User getUserByEmail(String email){
        return userRepo.findUsersByEmail(email);
    }

    public User create(User user){
        Optional<User> userByUsername =  userRepo.findUserByUsername(user.getUsername());
        Optional<User> userByEmail    =  userRepo.findUserByEmail(user.getEmail());
        logger.info("username: "+user.getUsername()+", and email: "+user.getEmail());

        System.out.println(user.toString());

        if(userByUsername.isPresent()) {
            throw new IllegalStateException("Username is already registered and taken");
        }

        System.out.println(user.toString());
        if(userByEmail.isPresent()) {
            throw new IllegalStateException("Email is already registered and taken");
        }

        return userRepo.save(user);
    }


    @Transactional
    public void delete(int id){
        boolean exists = userRepo.existsById(id);
        if( !exists) {
            throw new IllegalStateException("User with id: " + id+" does not exist in the database");
        }
        userRepo.deleteById(id);
    }

    @Transactional
    public int updateRole( int id, int newUserRole) {

        User user = userRepo.findById(id)
                            .orElseThrow(()-> new IllegalStateException(
                                    "User with id: "+ id+ "does not exists") );
        int userRole = user.getUserRole();
        if (userRole == newUserRole) {
            logger.info("Current user Role Id: {} same as new Role Id: {}, NO update required! ",
                        userRole, newUserRole);
            return 0;
        }
        logger.info("Current user Role Id: {} will be change to new Role Id: {} ", userRole, newUserRole);
        user.setUserRole(newUserRole);
        userRepo.save(user);
        return newUserRole;
    }

    @Transactional
    public User update(int id, User updatedUser){

        Optional<User> userByUsername =  userRepo.findUserByUsername(updatedUser.getUsername());
        Optional<User> userByEmail    =  userRepo.findUserByEmail(updatedUser.getEmail());
        logger.info("id = {} userByUsername = {} userByEmail: {} ",id,  userByUsername, userByEmail );
        System.out.println(updatedUser.toString());

        if(userByUsername.isPresent()) {
            logger.info("userByUsername = {} ", userByUsername);
            throw new IllegalStateException("Username is already registered and taken");
        }

        if(userByEmail.isPresent()) {
            logger.info(" userByEmail = {} ", userByEmail);
            throw new IllegalStateException("Email is already registered and taken");
        }

        User userToUpdate = userRepo.getOne(id);
        userToUpdate.setUsername(updatedUser.getUsername());
        userToUpdate.setEmail(updatedUser.getEmail());
        userToUpdate.setPassword(passwordSecurity.hashPass(updatedUser.getPassword()));

        userRepo.save(userToUpdate);
        return userToUpdate;
    }


    public List<UserDTO> getByUsername(String username){
        List<User> users = userRepo.findUsersByUsernameContaining(username);
        List<UserDTO> userDTO = new ArrayList<>();
        for (User user: users) {
            UserDTO userDto = new UserDTO();
            userDto.setId(user.getId());
            userDto.setUsername(user.getUsername());
            userDto.setEmail(user.getEmail());
            userDto.setUserRole(user.getUserRole());
            userDTO.add(userDto);
        }
        return userDTO;
    }

    public List<UserDTO> getByEmail(String email){
        List<User> users = userRepo.findUsersByEmailContaining(email);
        List<UserDTO> userDTO = new ArrayList<>();
        for (User user: users) {
            UserDTO userDto = new UserDTO();
            userDto.setId(user.getId());
            userDto.setUsername(user.getUsername());
            userDto.setEmail(user.getEmail());
            userDto.setUserRole(user.getUserRole());
            userDTO.add(userDto);
        }
        return userDTO;
    }

    /*
    ///////////////////
    DEPRACATED
    //////////////////
     */
//    // This method grabs all users
//    public List<UserDTO> getAllUsers() {
//
//        List<User> users = userRepo.findAll();
//        List<UserDTO> userDTOs = new ArrayList<>();
//        for (User user : users) {
//            UserDTO userDto = new UserDTO();
//            userDto.setId(user.getId());
//            userDto.setUsername(user.getUsername());
//            userDto.setEmail(user.getEmail());
//            userDto.setUserRole(user.getUserRole());
//            userDTOs.add(userDto);
//        }
//        return userDTOs;
//    }



    public List<UserGroupDTO> getAllUsersWithGroup() {
        List<User> users = userRepo.findAll();
        List<UserGroupDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            if (user.getGroups() != null) {
                UserGroupDTO userDto = new UserGroupDTO();
                userDto.setUsername((user.getUsername()));
                userDto.setId(user.getId());
                userDto.setGroups(user.getGroups());
                userDTOs.add(userDto);
            }
        };  //end of for loop
        return userDTOs;
    }


    // This method grabs all the users in a group specified by name
    public List<UserGroupDTO> getAllUsersInGroup(String name) {
        List<User> users = userRepo.findAll();
        List<UserGroupDTO> userDtos = new ArrayList<>();
        for (User user : users) {
            Group group;
            List<Group> groups = new ArrayList<>();
            group = checkForGroup(user, name);

            //if the user was a part of the group, create a DTO object to store and add to the list
            // otherwise, skip and move onto the next user.
            if (group != null) {

                UserGroupDTO userDto = new UserGroupDTO();
                userDto.setUsername(user.getUsername());
                userDto.setId(user.getId());
                userDto.setGroups(groups);
                userDtos.add(userDto);
            }
        }
        return userDtos;
    }

    // Grab all users in a group by group ID
    public List<UserGroupDTO> getAllUsersInGroupId(int id) {
        List<User> users = userRepo.findAll();
        List<UserGroupDTO> userDtos = new ArrayList<>();
        for (User user : users) {
            Group group;
            List<Group> groups = new ArrayList<>();
            group = checkForGroupId(user, id);

            //if the user was a part of the group, create a DTO object to store and add to the list
            // otherwise, skip and move onto the next user.
            if (group != null) {
                groups.add(group);
                UserGroupDTO userDto = new UserGroupDTO();
                userDto.setUsername(user.getUsername());
                userDto.setId(user.getId());
                userDto.setUserStatus(
                        userGroupRepo.findUniqueId(user.getId(), group.getId()).getStatus());
                userDto.setGroups(groups);
                userDtos.add(userDto);
            }
        }
        return userDtos;

    }


    // grab all groups that a user is a part of by group name
    public List<UserGroupDTO> getAllGroupsForUser(String name) {
        List<UserGroupDTO> groupsWithUsersDto = new ArrayList<>();
        UserGroupDTO userGroupDto = new UserGroupDTO();
        List<User> users = userRepo.findAll();
        for (User user : users) {
            if (user.getUsername().equals(name)) {
                List<Group> groups = user.getGroups();
                userGroupDto.setGroups(groups);
                userGroupDto.setUsername((user.getUsername()));
                userGroupDto.setId(user.getId());
            }
        }
        groupsWithUsersDto.add(userGroupDto);
        return groupsWithUsersDto;
    }

    // Grab all groups that a user is a part of by group id
    public List<UserGroupDTO> getAllGroupsForUserId(int id) {
        List<UserGroupDTO> groupsWithUsersDto = new ArrayList<>();
        UserGroupDTO userGroupDto = new UserGroupDTO();
        List<User> users = userRepo.findAll();
        for (User user : users) {
            if (user.getId() == id) {
                List<Group> groups = user.getGroups();
                groups.forEach(g -> {
                    g.setUserStatus(userGroupRepo.findUniqueId(id, g.getId()).getStatus());
                    g.setNumMembers(userGroupRepo.countByGroupId(g.getId()));
                });
                userGroupDto.setGroups(groups);
                userGroupDto.setUsername((user.getUsername()));
                userGroupDto.setId(user.getId());
            }
        }
        groupsWithUsersDto.add(userGroupDto);
        return groupsWithUsersDto;
    }

    // this method is called to check to see if a user is a part of a group given by a group name
    private Group  checkForGroup(User user, String name) {
        Group group = new Group();
        System.out.println("users groupsize = " + user.getGroups().size());
        for (int i = 0; i < user.getGroups().size(); i++) {

            // if a user is a part of the group of the given name, then return that group
            if (user.getGroups().get(i).getName().equals(name)) {
                group.setName(user.getGroups().get(i).getName());
                group.setId(user.getGroups().get(i).getId());
                return group;
            }
        }
        return null;
    }


    // this method is called to check to see if a user is a part of a group given by a group id
    private Group checkForGroupId(User user, int id) {
        Group group = new Group();
        for (int i = 0; i < user.getGroups().size(); i++) {

            // if a user is a part of the group of the given name, then return that group
            if (user.getGroups().get(i).getId() == id) {
                group.setName(user.getGroups().get(i).getName());
                group.setId(user.getGroups().get(i).getId());
                return group;
            }
        }
        return null;
    }
}


