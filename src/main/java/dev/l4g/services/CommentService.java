package dev.l4g.services;

//import dev.l4g.exceptions.ResourceNotFoundException;
import dev.l4g.models.Comment;
import dev.l4g.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepo;

    // Get All Comments in DB
    public List<Comment> getAll(){
        return commentRepo.findAll();
    }

    public Comment createComment(Comment comment) {
        return commentRepo.save(comment);
    }

    public List<Comment> getCommentsByDiscussionId(int id) {
        List<Comment> comments = commentRepo.findDiscussionByDiscussionId(id);
        if (comments.isEmpty()){
            throw new EntityNotFoundException("No comments found for discussion with id: " + id);
        }
        return comments;
    }

    public List<Comment> getRepliesByParentId(int id) {
        List<Comment> replies = commentRepo.findRepliesByParentId(id);
        if (replies.isEmpty()){
            throw new EntityNotFoundException("No replies found for comment with parentId: " + id);
        }
        return replies;
    }

    public List<Comment> getCommentsByUserId(int id) {
        List<Comment> comments = commentRepo.findDiscussionByUserId(id);
        if (comments.isEmpty()){
            throw new EntityNotFoundException("No comments found for user with id: " + id);
        }
        return comments;
    }

    // Edit a Comment
    public void editComment(Comment alteredComment) {
        Optional<Comment> option = commentRepo.findById(alteredComment.getId());
        Comment commentToEdit = option.get();
        if (commentToEdit == null){
            throw new EntityNotFoundException("No comment found");
        }
        commentToEdit.setMessage(alteredComment.getMessage());
        commentRepo.save(commentToEdit);
    }

    // Delete a comment
    @Transactional
    public void deleteComment(int id) {
        Optional<Comment> option = commentRepo.findById(id);
        Comment comment = option.get();
        if (comment == null) {
            throw new EntityNotFoundException("No comment found");
        }
        recursiveDeleteComment(comment);
        commentRepo.delete(comment);
    }

    public void recursiveDeleteComment(Comment c) {
        List<Comment> comments = commentRepo.findRepliesByParentId(c.getId());
        if(comments.isEmpty()) {
            commentRepo.delete(c);
            return;
        }
        comments.forEach(comment -> {
            recursiveDeleteComment(comment);
        });
        commentRepo.delete(c);
    }
}
