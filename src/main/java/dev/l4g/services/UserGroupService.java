package dev.l4g.services;

import dev.l4g.models.Group;
import dev.l4g.models.User;
import dev.l4g.models.UserGroup;
import dev.l4g.repositories.GroupRepository;
import dev.l4g.repositories.UserGroupRepository;
import dev.l4g.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class UserGroupService {

    @Autowired
    UserGroupRepository userGroupRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    GroupRepository groupRepo;

    public List<UserGroup> getAll() {
        return userGroupRepo.findAll();
    }

    // This method creates a group linked to a user
    public UserGroup createUserGroup(UserGroup newLink) {
        return userGroupRepo.save(newLink);
    }

    // Method to add a user to a given group, in JSON format
    public UserGroup addUserToGroup(UserGroup addUser) {
        if(userGroupRepo.findUniqueId(addUser.getUserId(), addUser.getGroupId()) != null)
            return null;
        return userGroupRepo.save(addUser);
    }

    public UserGroup userInGroup(int userId, int groupId) {

        //Retrieve User_ID of user to be deleted
        System.out.println(("userId" + userId +  " groupId " + groupId));
        Optional<User> option = userRepo.findById(userId);
        User userToCheck = option.get();
        UserGroup checkUser = new UserGroup();

        // Retrieve Group_ID of group for user to be removed from
        Optional<Group> groupOption = groupRepo.findById(groupId);
        Group group = groupOption.get();;
        checkUser.setUserId(userToCheck.getId());
        checkUser.setGroupId(group.getId());
        System.out.println("Check User: ID: " + checkUser.getUserId() + " groupID: " + checkUser.getGroupId());
        checkUser = userGroupRepo.findUniqueId(checkUser.getUserId(), checkUser.getGroupId());

        if (checkUser == null){
            throw new EntityNotFoundException("No user in this group");
        }
        return checkUser;
    }

    // A method to delete a user from a given group
    public void deleteUserFromGroup(int userId, int groupId) {

        //Retrieve User_ID of user to be deleted
        Optional<User> option = userRepo.findById(userId);
        User userToDelete = option.get();
        UserGroup deleteUser = new UserGroup();

        // Retrieve Group_ID of group for user to be removed from
        Optional<Group> groupOption = groupRepo.findById(groupId);
        Group group = groupOption.get();
        deleteUser.setUserId(userToDelete.getId());
        deleteUser.setGroupId(group.getId());

        // retrieve the ID from the Join table "UserGroup"
        deleteUser = userGroupRepo.findUniqueId(deleteUser.getUserId(), deleteUser.getGroupId());

        // Delete user from Group
        userGroupRepo.delete(deleteUser);
    }


    // makes user a moderator
    public void editModerator(UserGroup editMod) {
        UserGroup modStatus = userGroupRepo.findUniqueId(editMod.getUserId(), editMod.getGroupId());
        modStatus.setStatus(editMod.getStatus());
        System.out.println("In service: user status: " + modStatus.getStatus());
        userGroupRepo.save(modStatus);
    }

}
