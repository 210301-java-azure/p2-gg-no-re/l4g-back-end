package dev.l4g.dto;

public class UserDTO {

    private int id;
    private String username;
//    private String password;
    private int userRole;
    private String email;

    public UserDTO() {

    }

//    public UserDTO (int id, String username, int userRole, String email){
//        this.id = id;
//        this.username = username;
//        this.userRole = userRole;
//        this.email = email;
//    }

    public UserDTO(int id, String username, int userRole, String email) {
        this.id = id;
        this.username = username;
        this.userRole = userRole;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }

    public int getUserRole() {
        return userRole;
    }

    public void setUserRole(int userRole) {
        this.userRole = userRole;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

