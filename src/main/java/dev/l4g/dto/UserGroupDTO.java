package dev.l4g.dto;

import dev.l4g.models.Group;

import java.util.List;

public class UserGroupDTO {

    private int id;
    private String username;
    private int userStatus;
//    private Group group;
    private List<Group> groups;

    public UserGroupDTO() {
        //super();
    }

    public UserGroupDTO(int id, String username, List<Group> groups) {
        this.id = id;
        this.username = username;
        this.groups = groups;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
