package dev.l4g.util;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordSecurity {

    private static int workload = 12;

    // Function to Hash a given Password
    public String hashPass(String password) {
        String salt = BCrypt.gensalt(workload);
        String hashedPassword = BCrypt.hashpw(password, salt);
        return hashedPassword;
    }


    // Function to check a password against a hashed password
    public static boolean checkPassword(String password, String hashedPass) {
        boolean verified = false;
        verified = BCrypt.checkpw(password, hashedPass);
        return verified;
    }
}
