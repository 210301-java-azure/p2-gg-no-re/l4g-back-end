### QUERIES

use l4g;

-- Populate DB
INSERT INTO groups (name) VALUES ('SmashBros')
INSERT INTO groups (name) VALUES ('Dota')
INSERT INTO groups (name) VALUES ('Leage of Legends')

INSERT INTO discussion (group_id, title) VALUES (1, 'Falcon is Good')
INSERT INTO discussion (group_id, title) VALUES(1, 'Tier List')
INSERT INTO discussion (group_id, title) VALUES(2, 'Dota is bad')

DELETE FROM discussion WHERE id = 5

INSERT INTO users (username, password, user_role)
VALUES ('DuncaChunk', '12345', 0)
INSERT INTO users (username, password, user_role)
VALUES ('Smasher997', 'reeee', 2)
INSERT INTO users (username, password, user_role)
VALUES ('IplaySMASH', 'goblet', 2)
INSERT INTO users (username, password, user_role)
VALUES ('Johny5', 'password', 2)
INSERT INTO users (username, password, user_role)
VALUES ('ScaryTerry', 'GRAAAH', 2)


INSERT INTO comment (discussion_id, user_id, parent_id, message)
VALUES (1, 1, null, 'Falcon is totally rad')
INSERT INTO comment (discussion_id, user_id, parent_id, message)
VALUES (3, 1, null, 'Heres my Tier list...')

INSERT INTO comment (discussion_id, user_id, parent_id, message)
VALUES (1, 2, null, 'Nah falcon is TRASH')

INSERT INTO comment (discussion_id, user_id, parent_id, message)
VALUES (6, 2, null, 'Dota is NOT bad dude! dont say that!')
INSERT INTO comment (discussion_id, user_id, parent_id, message)
VALUES (6, 3, null, 'nah dog, ur trippin, DOTA IS BADDDD')

INSERT INTO user_group (group_id, user_id)
VALUES (1, 1)
INSERT INTO user_group (group_id, user_id)
VALUES (1, 4)
INSERT INTO user_group (group_id, user_id)
VALUES (1, 5)


-- TEST GRAB ALL USERS WHO'VE COMMENTED
SELECT * FROM users u
JOIN comment c ON u.id = c.user_id

-- TEST GRAB ALL USERS IN A DISCUSSION
-- Change the discussion_id to grab different users/discussion
-- Discussion id = 1 is: "Falcon is bad"
-- Discussion id = 3 is: "Smash Tier List"
-- Discussion id = 6 is: "Dota is bad"
SELECT username, c.message, c.discussion_id FROM users u
JOIN comment c ON c.user_id = u.id
WHERE discussion_id = 1

-- TEST GRAB ALL DISCUSSIONS IN A GROUP
-- group_id 1 = "Smashbros"
-- group_id 2 = "Dota"
SELECT * FROM groups g
JOIN discussion d ON g.id = d.group_id 
WHERE d.group_id = 1

--TEST GRAB ALL USERS IN A GROUP
SELECT u.username, g.name, g.id AS 'GROUP_ID', u.id AS 'user_id'
FROM user_group ug
JOIN groups g ON g.id = ug.group_id
JOIN users u ON u.id = ug.user_id
-- optional WHERE g.id = (group_id) to select group

SELECT *
FROM user_group ug
JOIN groups g ON g.id = ug.group_id
JOIN users u ON u.id = ug.user_id
-- optional WHERE g.id = (group_id) to select group

Select id AS 'user_group_id' , group_id, user_id FROM user_group







